# ldraw_cliff.html - Introduction

ldraw_cliff.html is a standalone HTML/ECMAScript application to generate LDraw
models (.LDR) of cliff landscapes.

By “standalone application” we mean that the user just needs to open the file
ldraw_cliff.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.

ldraw_cliff.html is licenced under GPLv3+.


# Requirements

* NONE!
